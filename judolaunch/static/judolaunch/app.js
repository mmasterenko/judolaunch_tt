var api_get_code = '/api/get_code';
var api_discounts = '/api/discounts';
var api_users = '/api/users';
var api_products = '/api/products';

var app = new Vue({
    el: '#app',
    data: {
        is_visible: false,
        current_code: '',
        current_user: null,
        current_product: null,
        users: [],
        products: []
    },
    created: function () {
        var vm = this;
        $.getJSON(api_users, function (data) {
            vm.users = data['results'];
        });
        $.getJSON(api_products, function (data) {
            vm.products = data['results'];
        });
    },
    methods: {
        changeVisibility: function () {
            this.is_visible = true
        },
        save_code: function (user, product, code) {
            var vm = this;
            $.post(api_discounts, {
                user: user,
                product: product,
                code: code
            }, function (data) {
                vm.current_code = data['code'];
                alert('Сгенерирован новый код: ' + data['code']);
            })
        },
        generate_code: function () {
            var vm = this;
            $.getJSON(api_get_code, function (data) {
                vm.save_code(vm.current_user, vm.current_product, data['code'])
            });
        }
    }
});
