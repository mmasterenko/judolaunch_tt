import random
from string import ascii_letters, digits

from django.db import models
from django.contrib.auth.models import User


CODE_POSSIBLE_CHARS = ascii_letters + digits


class Product(models.Model):
    title = models.CharField(max_length=128)

    def __str__(self):
        return self.title


class DiscountCode(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    code = models.CharField(max_length=7, unique=True)

    def __str__(self):
        return self.code

    @classmethod
    def generate_code(cls):
        def rs():  # rs means 'random symbol'
            return random.choice(CODE_POSSIBLE_CHARS)
        code_list = cls.objects.values_list('code', flat=True)
        code = '{}-{}-{}-{}'.format(rs(), rs(), rs(), rs())
        for _ in range(100):
            if code not in code_list:
                return code
        raise Exception('more than 100 attempts generate the random code')
