from django.contrib import admin

from .models import Product, DiscountCode


class DiscountCodeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'user', 'product']
    list_filter = ['user', 'product']


admin.site.register(Product)
admin.site.register(DiscountCode, DiscountCodeAdmin)
