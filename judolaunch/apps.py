from django.apps import AppConfig


class JudolaunchConfig(AppConfig):
    name = 'judolaunch'
