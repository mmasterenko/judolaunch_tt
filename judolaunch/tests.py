from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APITestCase

from .models import Product, DiscountCode


def create_user(username, password, **kwargs):
    user = User(username=username, **kwargs)
    user.set_password(password)
    user.save()
    return user


class TestAPI(APITestCase):

    def setUp(self):
        self.staff_user = create_user(username='test2', password='qwerty12345', is_staff=True)
        self.client.force_login(self.staff_user)

    def test_get_users(self):
        url = '/api/users'
        expected_response = {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "id": 1,
                    "username": "test2"
                }
            ]
        }
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(expected_response, response.data)

    def test_get_products(self):
        url = '/api/products'
        Product.objects.create(title='milk')
        Product.objects.create(title='bread')
        expected_response = {
            "count": 2,
            "next": None,
            "previous": None,
            "results": [
                {
                    "id": 1,
                    "title": "milk"
                },
                {
                    "id": 2,
                    "title": "bread"
                }
            ]
        }
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(expected_response, response.data)

    def test_get_discounts(self):
        url = '/api/discounts'
        product = Product.objects.create(title='milk')
        code = DiscountCode.generate_code()
        DiscountCode.objects.create(user=self.staff_user, product=product, code=code)
        expected_response = {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "user": self.staff_user.id,
                    "product": product.id,
                    "code": code
                }
            ]
        }
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(expected_response, response.data)

    def test_post_discounts(self):
        url = '/api/discounts'
        product = Product.objects.create(title='milk')
        code = DiscountCode.generate_code()
        data = {
            'user': self.staff_user.id,
            'product': product.id,
            'code': code
        }
        response = self.client.post(url, data)
        expected_response = {
            "user": self.staff_user.id,
            "product": product.id,
            "code": code
        }
        self.assertDictEqual(expected_response, response.data)

    def test_code(self):
        url = '/api/get_code'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('code', response.data)
        code = response.data['code']
        self.assertRegex(code, r'[a-zA-Z0-9]-[a-zA-Z0-9]-[a-zA-Z0-9]-[a-zA-Z0-9]')


class TestPages(TestCase):

    def setUp(self):
        self.test_user = create_user(username='test', password='qwerty12345', is_staff=False)
        self.client.force_login(self.test_user)

    def test_client_discount_page(self):
        url = '/discount'

        product_1 = Product.objects.create(title='milk')
        code_1 = DiscountCode.generate_code()

        product_2 = Product.objects.create(title='bread')
        code_2 = DiscountCode.generate_code()

        product_3 = Product.objects.create(title='sugar')
        code_3 = DiscountCode.generate_code()

        DiscountCode.objects.create(user=self.test_user, product=product_1, code=code_1)
        DiscountCode.objects.create(user=self.test_user, product=product_2, code=code_2)
        DiscountCode.objects.create(user=self.test_user, product=product_3, code=code_3)

        response = self.client.get(url)

        template = "<tr><td>{product}</td><td>{code}</td></tr>"
        needle_1 = template.format(product=product_1, code=code_1)
        needle_2 = template.format(product=product_2, code=code_2)
        needle_3 = template.format(product=product_3, code=code_3)

        self.assertInHTML(needle_1, response.rendered_content)
        self.assertInHTML(needle_2, response.rendered_content)
        self.assertInHTML(needle_3, response.rendered_content)
