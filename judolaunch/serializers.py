from rest_framework import serializers
from django.contrib.auth.models import User

from .models import DiscountCode, Product


class DiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscountCode
        fields = [
            'user',
            'product',
            'code',
        ]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
        ]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id',
            'title',
        ]
