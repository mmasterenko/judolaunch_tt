from django.views.generic import TemplateView, FormView, View, RedirectView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy

from .models import DiscountCode


class LoginView(FormView):
    template_name = 'judolaunch/login.html'
    form_class = AuthenticationForm
    success_url = reverse_lazy('root')

    def form_valid(self, form):
        login(self.request, form.get_user())
        return HttpResponseRedirect(self.get_success_url())


class LogOutView(RedirectView):
    url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)


class RootView(LoginRequiredMixin, View):
    login_url = reverse_lazy('login')

    def get(self, request):
        if self.request.user.is_staff:
            url = reverse('staff_discount')
        else:
            url = reverse('discount')
        return HttpResponseRedirect(redirect_to=url)


class DiscountView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('login')
    template_name = 'judolaunch/disount.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['codes'] = DiscountCode.objects.filter(user=self.request.user)
        return context


class StaffDiscountView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy('login')
    template_name = 'judolaunch/staff_discount.html'

    def test_func(self):
        return self.request.user.is_staff
