from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import SessionAuthentication
from rest_framework.pagination import LimitOffsetPagination

from django.contrib.auth.models import User

from .models import DiscountCode, Product
from .serializers import DiscountSerializer, ProductSerializer, UserSerializer


class GetCodeAPIView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    authentication_classes = (SessionAuthentication,)

    def get(self, request):
        code = DiscountCode.generate_code()
        return Response({'code': code})


class DiscountAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    authentication_classes = (SessionAuthentication, )
    pagination_class = LimitOffsetPagination
    serializer_class = DiscountSerializer
    queryset = DiscountCode.objects.all()


class UserAPIView(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    authentication_classes = (SessionAuthentication,)
    pagination_class = LimitOffsetPagination
    serializer_class = UserSerializer
    queryset = User.objects.all()


class ProductAPIView(ListAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    authentication_classes = (SessionAuthentication,)
    pagination_class = LimitOffsetPagination
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
