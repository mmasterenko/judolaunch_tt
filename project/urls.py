"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from judolaunch.views import LoginView, LogOutView, StaffDiscountView, DiscountView, RootView
from judolaunch.api_views import GetCodeAPIView, DiscountAPIView, UserAPIView, ProductAPIView

urlpatterns = [
    url(r'^$', RootView.as_view(), name='root'),
    url(r'^login', LoginView.as_view(), name='login'),
    url(r'^logout', LogOutView.as_view(), name='logout'),
    url(r'^discount', DiscountView.as_view(), name='discount'),
    url(r'^for_staff/discount', StaffDiscountView.as_view(), name='staff_discount'),
    url(r'^api/get_code', GetCodeAPIView.as_view(), name='api_get_code'),
    url(r'^api/discounts', DiscountAPIView.as_view(), name='api_discount'),
    url(r'^api/users', UserAPIView.as_view(), name='api_user'),
    url(r'^api/products', ProductAPIView.as_view(), name='api_product'),
    url(r'^admin/', admin.site.urls),
]
